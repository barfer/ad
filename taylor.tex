%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Arsclassica Article
% LaTeX Template
% Version 1.1 (10/6/14)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Lorenzo Pantieri (http://www.lorenzopantieri.net) with extensive modifications by:
% Vel (vel@latextemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[
10pt, % Main document font size
a4paper, % Paper type, use 'letterpaper' for US Letter paper
oneside, % One page layout (no page indentation)
%twoside, % Two page layout (page indentation for binding and different headers)
headinclude,footinclude, % Extra spacing for the header and footer
BCOR5mm, % Binding correction
]{scrartcl}

\input{structure.tex} % Include the structure.tex file which specified the document structure and layout
\usepackage{lmodern}
\usepackage[left=1.5in,right=1in,top=1in,bottom=1in]{geometry}
\setlength{\parskip}{0.15cm plus4mm minus3mm}
\newcommand{\remark}[1]{\qquad {\it Remark: #1}}

\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\Mod}{\ensuremath{\mathrm{mod}}}
\newcommand{\Exp}[1]{\ensuremath{\mathrm{e}^{#1}}}
\newcommand{\taylor}[2]{\ensuremath{{#1}_{#2}}}
\newcommand{\pseries}[4]{\ensuremath{\sum_{#2 = #3}^{#4} #1 (x - x_0)^{#2}}}
\newcommand{\tseries}[4]{\ensuremath{\pseries{\taylor{#1}{#2}}{#2}{#3}{#4}}}
\newcommand{\pdseries}[5]{\ensuremath{\sum_{#2 = #3}^{#4} #1 (x - x_0)^{#5}}}
\newcommand{\tdseries}[5]{\ensuremath{\pdseries{\taylor{#1}{#2}}{#2}{#3}{#4}{#5}}}



%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR(S)
%----------------------------------------------------------------------------------------

\title{\normalfont\spacedallcaps{Automatic Differentiation Rules for Univariate Taylor Series}} % The article title

\author{\spacedlowsmallcaps{Ferenc A. Bartha}} % The article author(s) - author affiliations need to be specified in the AUTHOR AFFILIATIONS block

\date{\small{Department of Computer Science, Rice University, PO Box 1892, MS-132,\\ Houston TX 77251, USA\\ \ \\Ferenc.A.Bartha@rice.edu}} % An optional date to appear under the author(s)

%----------------------------------------------------------------------------------------

\begin{document}

%----------------------------------------------------------------------------------------
%	HEADERS
%----------------------------------------------------------------------------------------

\renewcommand{\sectionmark}[1]{\markright{\spacedlowsmallcaps{#1}}} % The header for all pages (oneside) or for even pages (twoside)
%\renewcommand{\subsectionmark}[1]{\markright{\thesubsection~#1}} % Uncomment when using the twoside option - this modifies the header on odd pages
\lehead{\mbox{\llap{\small\thepage\kern1em\color{halfgray} \vline}\color{halfgray}\hspace{0.5em}\rightmark\hfil}} % The header style

\pagestyle{scrheadings} % Enable the headers specified in this block

%----------------------------------------------------------------------------------------
%	TABLE OF CONTENTS & LISTS OF FIGURES AND TABLES
%----------------------------------------------------------------------------------------

\maketitle % Print the title/author/date block

\setcounter{tocdepth}{2} % Set the depth of the table of contents to show sections and subsections only

\tableofcontents % Print the table of contents

%\listoffigures % Print the list of figures

%\listoftables % Print the list of tables

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\section*{Abstract} % This section will not appear in the table of contents due to the star (\section*)

AD formulae

%----------------------------------------------------------------------------------------

\section*{Overview}

We differentiate with respect to \(x\) The \(k\)-th Taylor coefficient of \(f(x)\) at \(x_0\) is denoted by \(\taylor{f}{k}\). 
All the expansions in this manuscript are considered to be expanded around the same \(x_0\). In an implementation we work 
with finite order that is \(f(x) \approx \tseries{f}{k}{0}{N}\). Unless it is required, no special rule 
is given for finite expansions.

% ------------------------------- ARITHMETIC OPERATIONS -------------------------------

\section{Arithmetic Operations}

\subsection{Addition \& Substraction}
Computing \(f \pm g\) gives 
\[\tseries{f}{k}{0}{\infty} \pm \tseries{g}{k}{0}{\infty} = \pseries{ \left( \taylor{f}{k} \pm \taylor{g}{k} \right) }{k}{0}{\infty}.\]
as taking the \(k\)-th Taylor coefficient is a linear operation. Thus,
\begin{equation}\label{AD-addsub}
	\taylor{(f \pm g)}{k} = \taylor{f}{k} \pm \taylor{g}{k} \qquad \mbox{for all } k.
\end{equation}

\subsection{Multiplication}
Computing \(f \times g\) gives 
\[\tseries{f}{k}{0}{\infty} \tseries{g}{k}{0}{\infty} = \pseries{ \left(\sum_{i = 0}^k \taylor{f}{i} \taylor{g}{k - i}\right) }{k}{0}{\infty}.\]
Thus,
\begin{equation}\label{AD-mul}
	\taylor{(f \times g)}{k} = \sum_{i = 0}^k \taylor{f}{i} \taylor{g}{k - i} \qquad \mbox{for all } k.
\end{equation}

\subsection{Division}
Note that the division is expandable only if \(f\) has at least as many vanishing Taylor coefficients at $x_0$ as $g$, that is $g_k = 0, k = 0 \ldots K ~ \Rightarrow ~ f_k = 0, k = 0 \ldots K$ for any $K \in \N$. Assume that the first non-zero coefficient of $g(x)$ is the $k_0$-th one.  That is $g(x) = \sum_{k=k_0}^\infty \taylor{g}{k} (x-x_0)^k$, thus $f(x) = \sum_{k=k_0}^\infty \taylor{f}{k} (x-x_0)^k$.
Writing formally \(f \div g\) gives 
\[\tseries{f}{k}{k_0}{\infty} \div \tseries{g}{k}{k_0}{\infty} = \tseries{(f \div g)}{k}{0}{\infty}.\]
Thus,
\[\tseries{f}{k}{k_0}{\infty} = \tseries{(f \div g)}{k}{0}{\infty} \tseries{g}{k}{k_0}{\infty} = \pseries{ \left(\sum_{i = 0}^{k - k_0} \taylor{(f \div g)}{i} \taylor{g}{k - i}\right) }{k}{k_0}{\infty}.\]
From this we get
\[ \taylor{(f \div g)}{k - k_0} = \frac{1}{\taylor{g}{k_0}}\left( \taylor{f}{k} - \left(\sum_{i = 0}^{k - k_0 - 1} \taylor{(f \div g)}{i} \taylor{g}{k - i}\right) \right) \qquad \mbox{for } k \geq k_0.\]
Shifting the indices results in 
\begin{equation}\label{AD-div}
	\taylor{(f \div g)}{k} = \frac{1}{\taylor{g}{k_0}}\left( \taylor{f}{k + k_0} - \left(\sum_{i = 0}^{k - 1} \taylor{(f \div g)}{i} \taylor{g}{k - i + k_0}\right) \right) \qquad \mbox{for all } k.
\end{equation}

\subsubsection*{Rule for finite Taylor expansions}
In \eqref{AD-div} the coefficient $\taylor{f}{k + k_0}$ appears together with the coefficients $\taylor{g}{k - i + k_0}, ~~ i = 0, \ldots, k - 1$. 
Due to finiteness, $\taylor{f}{k} = \taylor{g}{k} = 0$ for $k > N$. This results in
\begin{equation}\label{AD-div-finite}
	\taylor{(f \div g)}{k} = \begin{cases}
								\frac{1}{\taylor{g}{k_0}}\left( \taylor{f}{k + k_0} - \left(\sum_{i = 0}^{k - 1} \taylor{(f \div g)}{i} \taylor{g}{k - i + k_0}\right) \right) &\mbox{for } k \leq N - k_0,\\
								
								- \frac{1}{\taylor{g}{k_0}} \left( \sum_{i = k - (N - k_0)}^{k - 1} \taylor{(f \div g)}{i} \taylor{g}{k - i + k_0} \right) &\mbox{for } k > N - k_0.
							 \end{cases}
\end{equation}
% ------------------------------- EXPONENTIAL & LOGARITHM -------------------------------

\section{Exponential \& Logarithm}
We consider $g(x) = \tseries{g}{k}{0}{\infty}$. Note that this implies $g(x)' = \tdseries{kg}{k}{1}{\infty}{k-1}$.

\subsection{Exponential Function}
We have $\Exp{g(x)} = \tseries{(\Exp{g})}{k}{0}{\infty}$ and $(\Exp{g(x)})' = \tdseries{k(\Exp{g})}{k}{1}{\infty}{k - 1}$.

\noindent
As $(\Exp{g(x)})' = g(x)' \Exp{g(x)} \Rightarrow (x - x_0)(\Exp{g(x)})' = (x - x_0) g(x)' \Exp{g(x)}$, we get 
\[
\tseries{k(\Exp{g})}{k}{1}{\infty} = \tseries{kg}{k}{1}{\infty} \tseries{(\Exp{g})}{k}{0}{\infty} = 
\pseries{ \left( \sum_{i = 0}^k i \taylor{g}{i} \taylor{(\Exp{g})}{k - i} \right) }{k}{1}{\infty}\ .
\]
Coupling this with $\taylor{(\Exp{g})}{0} = \Exp{\taylor{g}{0}}$ 
and noting that $i = 0$ may be ignored, we obtain 
\begin{equation}\label{AD-exp}
	\taylor{(\Exp{g})}{k} =
		\begin{cases}
			\Exp{\taylor{g}{0}} &\mbox{for } k = 0, \\
			\frac{1}{k} \sum_{i = 1}^k i \taylor{g}{i} \taylor{(\Exp{g})}{k - i} &\mbox{for } k \geq 1.
		\end{cases}
\end{equation}

\subsection{Natural Logarithm}
We have $\ln{g(x)} = \tseries{(\ln{g})}{k}{0}{\infty}$ and $(\ln{g(x)})' = \tdseries{k(\ln{g})}{k}{1}{\infty}{k - 1}$.\\
Moreover, in order to expand the logarithm we need that $\taylor{g}{0} \neq 0$.

\noindent
Using that $(\ln{g(x)})' = \frac{g(x)'}{g(x)} \Rightarrow (x - x_0) g(x)' = (x - x_0) (\ln{g(x)})' g(x)$, we obtain 
$$\tseries{kg}{k}{1}{\infty} = \tseries{k(\ln{g})}{k}{1}{\infty} \tseries{g}{k}{0}{\infty} = 
\tseries{\left( \sum_{i = 0}^k i \taylor{(\ln{g})}{i} \taylor{g}{k - i} \right)}{k}{1}{\infty}\ .$$
Thus,
$k\taylor{g}{k} = \sum_{i = 1}^k i \taylor{(\ln{g})}{i} \taylor{g}{k - i}$. From this we get
$$\taylor{(\ln{g})}{k} = \frac{1}{ \taylor{g}{0} }\left( \taylor{g}{k} - \tfrac{1}{k} \sum_{i = 1}^{k-1} i \taylor{(\ln{g})}{i} \taylor{g}{k - i} \right)$$
for $k \geq 1$. Again, coupling this with $\taylor{(\ln{g})}{0} = \ln{\taylor{g}{0}}$ 
and noting that $i = 0$ may be ignored, we obtain 
\begin{equation}\label{AD-log}
	\taylor{(\ln{g})}{k} =
		\begin{cases}
			\ln{\taylor{g}{0}} &\mbox{for } k = 0, \\
			\frac{1}{ \taylor{g}{0} }\left( \taylor{g}{k} - \tfrac{1}{k} \sum_{i = 1}^{k-1} i \taylor{(\ln{g})}{i} \taylor{g}{k - i} \right) &\mbox{for } k \geq 1.
		\end{cases}
\end{equation}

% ------------------------------- POWER FUNCTION -------------------------------

\section{Power Function}

\subsection{Special Cases}
\begin{align}
	g(x) ^ 0 & = 1 \qquad \mbox {if } g(x) \neq 0,\\
	g(x) ^ 1 & = g(x),\\
	0 ^ {h(x)} & = 0 \qquad \mbox {if } h(x) \neq 0,\\
	1 ^ {h(x)} & = 1\\
\end{align}

\subsection{Square}
The square as a particular case of real power may be written in a much simpler form. As it is used 
in many applications it makes sense to treat it separately. The formula is derived from the multiplication
by coupling the repeated terms.
\begin{equation}\label{AD-square}
	\taylor{(g^2)}{k} = 2 \sum_{i = 0}^{\tfrac{k + (k ~ \Mod ~ 2) - 2}{2}} \taylor{g}{i} \taylor{g}{k - i} + 
	\begin{cases}
		\taylor{g}{k/2}^2 &\mbox{if } k ~ \Mod ~ 2 = 0,\\
		0                 &\mbox{if } k ~ \Mod ~ 2 = 1.
	\end{cases}
\end{equation}
 
\subsection{Square Root}
We derive the rule for square root from \eqref{AD-square}. We know that in order to 
expand $\sqrt{g}$ we need the first non-vanishing Taylor coefficient of $g$ to be of an 
even order if exists. Assume that the first non-zero coefficient of $g(x)$ is the $k_0$-th one   
that is $g(x) = \sum_{k=k_0}^\infty \taylor{g}{k} (x-x_0)^k$ and $k_0$ is even.
As
\[\tseries{g}{k}{k_0}{\infty} = \left( \tseries{(\sqrt{g})}{k}{0}{\infty} \right)^2, \]
we have $\taylor{(\sqrt{g})}{k} = 0$ for $k < \tfrac{k_0}{2}$, 
$\taylor{(\sqrt{g})}{k_0 / 2} = \sqrt{\taylor{g}{k_0}}$. We also have that 
\[\taylor{g}{k} = 2 \sum_{i = k_0 / 2}^{\tfrac{k + (k ~ \Mod ~ 2) - 2}{2}} \taylor{(\sqrt{g})}{i} \taylor{(\sqrt{g})}{k - i} + 
	\begin{cases}
		\taylor{(\sqrt{g})}{k/2}^2	&\mbox{if } k ~ \Mod ~ 2 = 0,\\
		0                 			&\mbox{if } k ~ \Mod ~ 2 = 1.
	\end{cases}\]
Therefore, 
\begin{equation}\label{AD-sqrt}
\begin{split}
\taylor{(\sqrt{g})}{k} &= \begin{cases}
							0 						&\mbox{for } k < \tfrac{k_0}{2},\\
							\sqrt{\taylor{g}{k_0}}	&\mbox{for } k = \tfrac{k_0}{2}
						 \end{cases}
						 \\
						 &\mbox{and}\\
\taylor{(\sqrt{g})}{k - k_0/2} &= \frac{1}{2 \taylor{(\sqrt{g})}{k_0/2}} \Big( \tiny \taylor{g}{k} - 2 \sum_{i = k_0 / 2 + 1}^{\tfrac{k + (k ~ \Mod ~ 2) - 2}{2}} \taylor{(\sqrt{g})}{i} \taylor{(\sqrt{g})}{k - i} - 
	\begin{cases}
		\taylor{(\sqrt{g})}{k/2}^2 	&\mbox{if } k ~ \Mod ~ 2 = 0\\
		0                 			&\mbox{if } k ~ \Mod ~ 2 = 1
	\end{cases}
	\Big)\\
	&\mbox{for } k > k_0.
\end{split}
\end{equation}

\subsubsection*{Rule for finite Taylor expansions}
In \eqref{AD-sqrt} the coefficient $\taylor{g}{k}$ appears. Due to finiteness, $\taylor{g}{k} = 0$ for $k > N$. Thus, 
\begin{equation}\label{AD-sqrt-finite}
\begin{split}
\taylor{(\sqrt{g})}{k} &= \begin{cases}
							0 						&\mbox{for } k < \tfrac{k_0}{2},\\
							\sqrt{\taylor{g}{k_0}}	&\mbox{for } k = \tfrac{k_0}{2}
						 \end{cases}\\
	&\mbox{and}\\
\taylor{(\sqrt{g})}{k - k_0/2} &= \frac{1}{2 \taylor{(\sqrt{g})}{k_0/2}} \Bigg( \tiny \taylor{g}{k} - 2 \sum_{i = k_0 / 2 + 1}^{\tfrac{k + (k ~ \Mod ~ 2) - 2}{2}} \taylor{(\sqrt{g})}{i} \taylor{(\sqrt{g})}{k - i} - 
	\begin{cases}
		\taylor{(\sqrt{g})}{k/2}^2	&\mbox{if } k ~ \Mod ~ 2 = 0\\
		0                 			&\mbox{if } k ~ \Mod ~ 2 = 1
	\end{cases}
	\Bigg)\\
	&\qquad \mbox{for } N \geq k > k_0\\
	&\mbox{and}\\
\taylor{(\sqrt{g})}{k - k_0/2} &= \frac{1}{2 \taylor{(\sqrt{g})}{k_0/2}} \Bigg( \tiny - 2 \sum_{i = k_0 / 2 + 1}^{\tfrac{k + (k ~ \Mod ~ 2) - 2}{2}} \taylor{(\sqrt{g})}{i} \taylor{(\sqrt{g})}{k - i} - 
	\begin{cases}
		\taylor{(\sqrt{g})}{k/2}^2	&\mbox{if } k ~ \Mod ~ 2 = 0\\
		0                 			&\mbox{if } k ~ \Mod ~ 2 = 1
	\end{cases}
	\Bigg)\\
	&\qquad \mbox{for } N + k_0 / 2 \geq k > N.
\end{split}
\end{equation}

\subsection{Integer Power}
Integer power \( g(x)^n, n \in \Z \) may be reduced to the repeated use of the square function.
\begin{equation}\label{AD-power-int}
\begin{split}
	g^n &= \begin{cases}
						\tfrac{1}{g^{-n}}	&\mbox{for } n < 0,\\
						1					&\mbox{for } n = 0, g \neq 0,\\
						g			 		&\mbox{for } n = 1,\\
						g^2				    &\mbox{for } n = 2\\
					  \end{cases}\\
		&\mbox{and}\\
	\taylor{(g^n)}{k} &=
	\taylor{
		\left((g^{\lfloor n / 2 \rfloor})^2 \times
			\begin{cases}
				1 &\mbox{if } n ~ \Mod ~ 2 = 0\\
				g &\mbox{if } n ~ \Mod ~ 2 = 1
			\end{cases}\right)
	}{k} \qquad \mbox{for } n \geq 3. \\
\end{split}	
\end{equation}
 

\subsection{Real Power}
The goal is to compute the Taylor series of $g(x)^a, \ a \in \R$ in the general case 
that is when $g(x)$ might have $0$ as the first Taylor coefficient. 

\noindent
Assume that the first non-zero coefficient of $g(x)$ is the $k_0$-th one that is $g(x) = \sum_{k=k_0}^\infty \taylor{g}{k} (x-x_0)^k$.
We consider the expansion for the power as $g(x)^a = \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l$.

\noindent
We have $(g(x)^a)' = a g(x)' g(x)^{a-1} \Rightarrow (x - x_0) g(x) (g(x)^a)' = (x - x_0) a g(x)' g(x)^a$. Write in the expansions, simplify and use the rule for multiplication
that is $\taylor{(f g)}{k} = \sum_{i=0}^k \taylor{f}{i}\taylor{g}{k-i}$.

\subsubsection*{Case: $k_0 = 0$}
\begin{equation*}\begin{split}
	(x - x_0) g(x) (g(x)^a)' &= 
	(x - x_0) \left( \sum_{k=0}^\infty \taylor{g}{k} (x-x_0)^k \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)' =\\
	&= \left( \sum_{k=0}^\infty \taylor{g}{k} (x-x_0)^k \right) (x - x_0) \left( \sum_{l=1}^\infty \taylor{(g^a)}{l} l (x-x_0)^{l-1} \right) =\\
	&= \left( \sum_{k=0}^\infty \taylor{g}{k} (x-x_0)^k \right) \left( \sum_{l=1}^\infty \taylor{(g^a)}{l} l (x-x_0)^{l} \right) =\\
	&= \left( \sum_{k=1}^\infty \left( \sum_{i = 0}^k \taylor{g}{i} \taylor{(g^a)}{k - i} (k - i) \right) (x-x_0)^k \right)
\end{split}\end{equation*}
\qquad and
\begin{equation*}\begin{split}
	(x - x_0) a g(x)' g(x)^a &=
	(x - x_0) a \left( \sum_{k=0}^\infty \taylor{g}{k} (x-x_0)^k \right)' \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right) =\\
	&= (x - x_0) a \left( \sum_{k=1}^\infty \taylor{g}{k} k (x-x_0)^{k-1} \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)=\\
	&= a \left( \sum_{k=1}^\infty \taylor{g}{k} k (x-x_0)^{k} \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)=\\
	&= \left( \sum_{k=1}^\infty \left( \sum_{i = 0}^k a \taylor{g}{i} i \taylor{(g^a)}{k - i} \right) (x-x_0)^k \right).
\end{split}\end{equation*}
From these we obtain for $k \geq 1$ that 
$$\sum_{i = 0}^k \taylor{g}{i} \taylor{(g^a)}{k - i} (k - i) = \sum_{i = 0}^k a \taylor{g}{i} i \taylor{(g^a)}{k - i}$$
that is (grouping by $\taylor{(g^a)}{k - i}$ and expressing for $\taylor{(g^a)}{k}$)
$$\taylor{(g^a)}{k} = \frac{\sum_{i = 1}^{k} \left(a \cdot i - (k - i) \right) \taylor{g}{i} \taylor{(g^a)}{k - i}}{k \cdot \taylor{g}{0}} = 
\frac{1}{\taylor{g}{0}} \sum_{i = 1}^{k} \left( \tfrac{(a + 1) i}{k} - 1 \right) \taylor{g}{i} \taylor{(g^a)}{k - i}\ .$$
Thus,
\begin{equation}\label{AD-power-nonzero}
	\taylor{(g^a)}{k} =
		\begin{cases}
			\taylor{g}{0} ^ a 																	&\mbox{for } k = 0, \\
			\frac{1}{\taylor{g}{0}} \sum_{i = 1}^{k} \left( \tfrac{(a + 1) i}{k} - 1 \right) 
				\taylor{g}{i} \taylor{(g^a)}{k - i} 											&\mbox{for } k \geq 1.
		\end{cases}
\end{equation}

\subsubsection*{Case: $k_0 \geq 1$}
\begin{equation*}\begin{split}
	(x - x_0) g(x) (g(x)^a)' &= 
	(x - x_0) \left( \sum_{k=k_0}^\infty \taylor{g}{k} (x-x_0)^k \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)' =\\
	&= (x-x_0)^{k_0} \left( \sum_{k=0}^\infty \taylor{g}{k + k_0} (x-x_0)^k \right) (x - x_0) \left( \sum_{l=1}^\infty \taylor{(g^a)}{l} l (x-x_0)^{l - 1} \right) =\\
	&= (x-x_0)^{k_0} \left( \sum_{k=0}^\infty \taylor{g}{k + k_0} (x-x_0)^k \right) \left( \sum_{l=1}^\infty \taylor{(g^a)}{l} l (x-x_0)^{l} \right) =\\
	&= \left( \sum_{k=1}^\infty \left( \sum_{i = 0}^k \taylor{g}{i + k_0} \taylor{(g^a)}{k - i} (k - i) \right) (x-x_0)^{k + k_0} \right)
\end{split}\end{equation*}
\qquad and
\begin{equation*}\begin{split}
	(x - x_0) a g(x)' g(x)^a &=
	(x - x_0) a \left( \sum_{k=k_0}^\infty \taylor{g}{k} (x-x_0)^k \right)' \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right) =\\
	&= a (x - x_0) \left( \sum_{k=k_0}^\infty \taylor{g}{k} k (x-x_0)^{k - 1} \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)=\\
	&= a \left( \sum_{k=k_0}^\infty \taylor{g}{k} k (x-x_0)^{k} \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)=\\
	&= a (x-x_0)^{k_0} \left( \sum_{k=0}^\infty \taylor{g}{k + k_0} (k + k_0) (x-x_0)^{k} \right) \left( \sum_{l=0}^\infty \taylor{(g^a)}{l} (x-x_0)^l \right)=\\
	&= \left( \sum_{k=0}^\infty \left( \sum_{i = 0}^k a \taylor{g}{i + k_0} (i + k_0) \taylor{(g^a)}{k - i} \right) (x-x_0)^{k + k_0} \right).
\end{split}\end{equation*}
Note that the power function is expandable around $0$ only if $a k_0$ is integer. Moreover all coefficients of $g^a$ up to $a k_0$ will be zero.
Thus, the sums may be taken as $\sum_{k = a k_0} ^ \infty$ that is 
$$(x - x_0) g(x) (g(x)^a)' = \left( \sum_{k=a k_0}^\infty \left( \sum_{i = 0}^k \taylor{g}{i + k_0} \taylor{(g^a)}{k - i} (k - i) \right) (x-x_0)^{k + k_0} \right)$$
\qquad and
$$(x - x_0) a g(x)' g(x)^a = \left( \sum_{k=a k_0}^\infty \left( \sum_{i = 0}^k a \taylor{g}{i + k_0} (i + k_0) \taylor{(g^a)}{k - i} \right) (x-x_0)^{k + k_0} \right).$$
From these we obtain for $k \geq a k_0 + 1$ that
$$\sum_{i = 0}^k \taylor{g}{i + k_0} \taylor{(g^a)}{k - i} (k - i) = \sum_{i = 0}^k a \taylor{g}{i + k_0} (i + k_0) \taylor{(g^a)}{k - i}$$
that is (grouping by $\taylor{(g^a)}{k - i}$ and expressing for $\taylor{(g^a)}{k}$)
$$\taylor{(g^a)}{k} = \frac{\sum_{i = 1}^k (a (i + k_0) - (k - i)) \taylor{g}{i + k_0} \taylor{(g^a)}{k - i} }{ (k - a k_0) \taylor{g}{k_0}} = 
\frac{1}{\taylor{g}{k_0}} \sum_{i = 1}^k \left( \tfrac{(a + 1)i}{k - a k_0} - 1 \right) \taylor{g}{i + k_0} \taylor{(g^a)}{k - i}.$$
\remark{Note the role of the division by $k - a k_0$ in the formula.}
 
\noindent
Thus,
\begin{equation}\label{AD-power-zero}
	\taylor{(g^a)}{k} =
		\begin{cases}
			0 													&\mbox{for } k < a k_0, \\
			\taylor{g}{k_0} ^ a 								&\mbox{for } k = a k_0,\\
			\frac{1}{\taylor{g}{k_0}} \sum_{i = 1}^{k} 
				\left( \tfrac{(a + 1) i}{k - a k_0} - 1 \right) 
				\taylor{g}{i + k_0} \taylor{(g^a)}{k - i} 		&\mbox{for } k \geq a k_0 + 1.
		\end{cases}
\end{equation}
Note that \eqref{AD-power-zero} gives back \eqref{AD-power-nonzero} in the case of $k_0 = 0$.

\subsubsection*{Rule for finite Taylor expansions}
When working with finite expansions ($0$ to $N$), we need to take care what coefficients are available directly. 
In \eqref{AD-power-zero} for $\taylor{(g^a)}{k}$, the coefficients $\taylor{g}{i + k_0}$ appear for $i = 1, \ldots, k$. 
Due to finiteness, $\taylor{g}{l} = 0$ for $l > N$. This results in
\begin{equation}\label{AD-real-power}
	\taylor{(g^a)}{k} =
		\begin{cases}
			0 																	&\mbox{for } k < a k_0, \\
			\taylor{g}{k_0} ^ a 												&\mbox{for } k = a k_0,\\
			\frac{1}{\taylor{g}{k_0}} \sum_{i = 1}^{\min\{k, N - k_0\}} 
				\left( \tfrac{(a + 1) i}{k - a k_0} - 1 \right) 
				\taylor{g}{i + k_0} \taylor{(g^a)}{k - i} 						&\mbox{for } N \geq k \geq a k_0 + 1.
		\end{cases}
\end{equation}

\subsection{Generic Rule for Power}
We compute $g(x)^{h(x)}$ by using rules \eqref{AD-exp} and \eqref{AD-log} that is
\begin{equation}\label{AD-power}
	\taylor{(g^h)}{k} = \taylor{(\Exp{h \, \ln{g}})}{k} \qquad \mbox{for all } k.
\end{equation}

% ------------------------------- TRIGONOMETRIC FUNCTIONS -------------------------------

\section{Trigonometric Functions}
\subsection{Sin \& Cos}
We shall see that $\sin{g(x)}$ and $\cos{g(x)}$ must be computed in parallel. Note that 
\[(\sin{g(x)})' = \cos{g(x)} g(x)' \Rightarrow (x - x_0) (\sin{g(x)})' = (x - x_0) \cos{g(x)} g(x)'.\]
Thus,
\[ \pseries{ k \taylor{(\sin{g})}{k} }{k}{1}{\infty} = \tseries{(\cos{g})}{k}{0}{\infty} \pseries{ l \taylor{g}{l} }{l}{1}{\infty} = 
\pseries{ \Big( \sum_{i = 1}^{k} i \taylor{g}{i} \taylor{(\cos{g})}{k - i} \Big) }{k}{1}{\infty}.
\]
that yields
\begin{equation}\label{AD-sin}
	\taylor{(\sin{g})}{k} = 
	\begin{cases}
		\sin{\taylor{g}{0}}														&\mbox{for } k = 0,\\
		\tfrac{1}{k} \sum_{i = 1}^{k} i \taylor{g}{i} \taylor{(\cos{g})}{k - i} &\mbox{for } k > 0.\\
	\end{cases}
\end{equation}

\noindent
Similarily, 
\[(\cos{g(x)})' = - \sin{g(x)} g(x)' \Rightarrow (x - x_0) (\cos{g(x)})' = - (x - x_0) \sin{g(x)} g(x)'.\]
Thus,
\[ \pseries{ k \taylor{(\cos{g})}{k} }{k}{1}{\infty} = - \tseries{(\sin{g})}{k}{0}{\infty} \pseries{ l \taylor{g}{l} }{l}{1}{\infty} = 
- \pseries{ \Big( \sum_{i = 1}^{k} i \taylor{g}{i} \taylor{(\cos{g})}{k - i} \Big) }{k}{1}{\infty}.
\]
that yields
\begin{equation}\label{AD-cos}
	\taylor{(\cos{g})}{k} = 
	\begin{cases}
		\cos{\taylor{g}{0}}														  &\mbox{for } k = 0,\\
		- \tfrac{1}{k} \sum_{i = 1}^{k} i \taylor{g}{i} \taylor{(\sin{g})}{k - i} &\mbox{for } k > 0.\\
	\end{cases}
\end{equation}

\subsection{Tan}
We present two slightly different options to derive the rule for \(\tan{g}\). 
We note that in our tests the two formulae produced almost identical results. In certain cases 
the first option was more accurate, but mostly the second formula deemed to be 
more precise that is it accumulated less rounding errors.
 
\subsubsection*{Option \(1\)}
As \(\tan{g} = \tfrac{\sin{g}}{\cos{g}}\) we may 
use the formula \eqref{AD-div} for division and get 
\begin{equation}\label{AD-tan-1a}
	\taylor{(\tan{g})}{k} = \begin{cases}
								\tan{\taylor{g}{0}}					&\mbox{for } k = 0, \\
								\taylor{(\sin{g} \div \cos{g})}{k}  &\mbox{for } k > 0.
							 \end{cases}
\end{equation}
Note that using \eqref{AD-div} for \(k = 0\) would correctly give 
\(\taylor{(\tan{g})}{0} = \taylor{(\sin{g} \div \cos{g})}{0}\). However, in an implementation, 
the built-in \(\tan\) function of the scalar type is expected to provide more accuracy.

\noindent
As \(\sin\) and \(\cos\) have disjoint zeros, one may also consider to 
inline the formula \eqref{AD-div} for division with \(k_0 = 0\) and get 
\begin{equation}\label{AD-tan-1b}
	\taylor{(\tan{g})}{k} = \begin{cases}
								\tan{\taylor{g}{0}}										&\mbox{for } k = 0, \\
								\frac{1}{\taylor{g}{0}}\left( \taylor{(\sin{g})}{k} - 
									\left(\sum_{i = 0}^{k - 1} \taylor{(\tan{g})}{i} 
									\taylor{(\cos{g})}{k - i}\right) \right) 			&\mbox{for } k > 0.
							 \end{cases}
\end{equation}

\subsubsection*{Option \(2\)}
As \((\tan{g})' = \tfrac{g'}{\cos^2{g}}\), we get 
\((x - x_0)(\tan{g})' (\cos^2{g}) = (x - x_0) g'\), thus, 
\[
	\pseries{ \left( \sum_{i = 1}^{k} i \taylor{(\tan{g})}{i} \taylor{(\cos^2{g})}{k - i} \right) }{k}{1}{\infty} = 
	\pseries{ k \taylor{g}{k} }{k}{1}{\infty}.
\]
Grouping \( \taylor{(\tan{g})}{k} \) to the left side, we obtain for \(k \geq 1\) that 
\[
	\taylor{(\tan{g})}{k} = \frac{1}{\taylor{(\cos^2{g})}{0}} \left( \taylor{g}{k} - 
	\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\tan{g})}{i} \taylor{(\cos^2{g})}{k - i} \right)\right).
\]
Using \(\taylor{(\cos^2{g})}{0} = 1 + \cos^2{\taylor{g}{0}}\) leads to 
\begin{equation}\label{AD-tan-2}
	\taylor{(\tan{g})}{k} = 
	\begin{cases}
		\tan{\taylor{g}{0}}														&\mbox{for } k = 0, \\
		\frac{1}{\cos^2{\taylor{g}{0}}} \left( \taylor{g}{k} - 
		\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\tan{g})}{i} 
			\taylor{(\cos^2{g})}{k - i} \right)\right) 							&\mbox{for } k > 0.
	\end{cases}
\end{equation}



\subsection{ArcSin}
As \((\arcsin{g})' = \tfrac{g'}{\sqrt{1 - g^2}}\) for \(-1 < g < 1\), we get 
\((x - x_0)(\arcsin{g})' \sqrt{1 - g^2} = (x - x_0) g'\), thus, 
\[
	\pseries{ \left( \sum_{i = 1}^{k} i \taylor{(\arcsin{g})}{i} \taylor{(\sqrt{1 - g^2})}{k - i} \right) }{k}{1}{\infty} = \pseries{ k \taylor{g}{k} }{k}{1}{\infty}.
\]
Grouping \( \taylor{(\arcsin{g})}{k} \) to the left side, we obtain for \(k \geq 1\) that 
\[
	\taylor{(\arcsin{g})}{k} = \frac{1}{\taylor{(\sqrt{1 - g^2})}{0}} \left( \taylor{g}{k} - 
	\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\arcsin{g})}{i} \taylor{(\sqrt{1 - g^2})}{k - i} \right)\right).
\]
Using \(\taylor{(\sqrt{1 - g^2})}{0} = \sqrt{1 - (\taylor{g}{0})^2}\) leads to 
\begin{equation}\label{AD-arcsin}
	\taylor{(\arcsin{g})}{k} = 
	\begin{cases}
		\arcsin{\taylor{g}{0}}												&\mbox{for } k = 0, \\
		\frac{1}{\sqrt{1 - (\taylor{g}{0})^2}} \left( \taylor{g}{k} - 
		\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\arcsin{g})}{i} 
		\taylor{(\sqrt{1 - g^2})}{k - i} \right)\right) 					&\mbox{for } k > 0. \\
	\end{cases}
\end{equation}

\subsection{ArcCos}
As \((\arccos{g})' = - \tfrac{g'}{\sqrt{1 - g^2}}\) for \(-1 < g < 1\), we get 
\((x - x_0)(\arccos{g})' \sqrt{1 - g^2} = - (x - x_0) g'\), thus, 
\[
	\pseries{ \left( \sum_{i = 1}^{k} i \taylor{(\arccos{g})}{i} \taylor{(\sqrt{1 - g^2})}{k - i} \right) }{k}{1}{\infty} = - \pseries{ k \taylor{g}{k} }{k}{1}{\infty}.
\]
Grouping \( \taylor{(\arccos{g})}{k} \) to the left side, we obtain for \(k \geq 1\) that 
\[
	\taylor{(\arccos{g})}{k} = - \frac{1}{\taylor{(\sqrt{1 - g^2})}{0}} \left( \taylor{g}{k} + 
	\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\arccos{g})}{i} \taylor{(\sqrt{1 - g^2})}{k - i} \right)\right).
\]
Using \(\taylor{(\sqrt{1 - g^2})}{0} = \sqrt{1 - (\taylor{g}{0})^2}\) leads to 
\begin{equation}\label{AD-arccos}
	\taylor{(\arccos{g})}{k} = 
	\begin{cases}
		\arccos{\taylor{g}{0}}												&\mbox{for } k = 0, \\
		\frac{- 1}{\sqrt{1 - (\taylor{g}{0})^2}} \left( \taylor{g}{k} + 
		\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\arccos{g})}{i} 
		\taylor{(\sqrt{1 - g^2})}{k - i} \right)\right) 					&\mbox{for } k > 0. \\
	\end{cases}
\end{equation}

\subsection{ArcTan}
As \((\arctan{g})' = \tfrac{g'}{1 + g^2}\), we get 
\((x - x_0)(\arctan{g})' (1 + g^2) = (x - x_0) g'\), thus, 
\[
	\pseries{ \left( \sum_{i = 1}^{k} i \taylor{(\arctan{g})}{i} \taylor{(1 + g^2)}{k - i} \right) }{k}{1}{\infty} = \pseries{ k \taylor{g}{k} }{k}{1}{\infty}.
\]
Grouping \( \taylor{(\arctan{g})}{k} \) to the left side, we obtain for \(k \geq 1\) that 
\[
	\taylor{(\arctan{g})}{k} = \frac{1}{\taylor{(1 + g^2)}{0}} \left( \taylor{g}{k} - 
	\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\arctan{g})}{i} \taylor{(1 + g^2)}{k - i} \right)\right).
\]
Using \(\taylor{(1 + g^2)}{0} = 1 + (\taylor{g}{0})^2\) leads to 
\begin{equation}\label{AD-arctan}
	\taylor{(\arctan{g})}{k} = 
	\begin{cases}
		\arctan{\taylor{g}{0}}												&\mbox{for } k = 0, \\
		\frac{1}{1 + (\taylor{g}{0})^2} \left( \taylor{g}{k} - 
		\frac{1}{k} \left( \sum_{i = 1}^{k - 1} i \taylor{(\arctan{g})}{i} 
			\taylor{(1 + g^2)}{k - i} \right)\right) 						&\mbox{for } k > 0.
	\end{cases}
\end{equation}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\renewcommand{\refname}{\spacedlowsmallcaps{References}} % For modifying the bibliography heading

\bibliographystyle{unsrt}

\bibliography{sample.bib} % The file containing the bibliography

%----------------------------------------------------------------------------------------

\end{document}
